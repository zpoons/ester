describe("wako module", function () {
    beforeEach(function () {
        browser.get("http://localhost/ChainflexReadycable#/");
    });
    afterEach(function () {
        //browser.executeScript('window.localStorage.clear()');
        browser.executeScript('window.sessionStorage.clear()');
    });
    it("'adding to cart' should show err popup if http error", function () {
        //msgpopup
        element(by.model("sc.sp.articleId")).sendKeys("2090UX");
        element(by.buttonText('Suchen')).click();
        element.all(by.repeater('item in sc.getSearchResults() track by $index')).first().click();
        element(by.partialButtonText("Weiter")).click();
        element(by.model("wc.wakoDetails().length")).clear();
        element(by.partialButtonText("Warenkorb")).click();
        var e = element(by.className("modal ng-isolate-scope msgpopup in"));
        expect(e.isDisplayed())
            .toBe(true);
        expect(e.element(by.className("whole"))
            .element(by.tagName('p')).getText()).toContain("Fehler");
    });
});
