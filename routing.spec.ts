describe("routing", () => {

    describe("details state", () => {

        beforeEach(() => {
            browser.get("http://localhost/ChainflexReadycable#/");
        });

        afterEach(() => {
            //browser.executeScript('window.localStorage.clear()');
            browser.executeScript('window.sessionStorage.clear()');
        });

        it("should detect empty wako length value", () => {
            element(by.model("sc.sp.articleId")).sendKeys("2090UX");
            element(by.model("sc.sp.length")).clear();
            element(by.buttonText('Suchen')).click();
            element.all(by.repeater('item in sc.getSearchResults() track by $index')).first().click();
            element(by.partialButtonText("Weiter")).click();
            expect(element(by.model("wc.wakoDetails().length")).getAttribute('value')).toBe("1");
        });

    });

})