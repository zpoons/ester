exports.config = {
    framework: 'jasmine',
    specs: ['*.spec.js'],
    seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    multiCapabilities: [
        {
            'browserName': 'chrome'
            //,
            //'chromeOptions': {
            //    'args': ['show-fps-counter=true']
            //}
        }
    //,
    //{
    //    'browserName': 'firefox'
    //}
    //,
    //{
    //    'browserName': 'internet explorer'
    //}
    ]
}