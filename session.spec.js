function search(suchString) {
    var sInput = element(by.model("sc.sp.articleId"));
    sInput.clear();
    sInput.sendKeys(suchString);
    var b = element(by.buttonText('Suchen'));
    return b.click();
}
function refresh() { return browser.refresh(); }
;
function m(s) { return element(by.model(s)); }
function results() {
    return element.all(by.repeater('item in sc.getSearchResults() track by $index'));
}
describe("sessionStore", function () {
    beforeEach(function () {
        browser.get("http://localhost/ChainflexReadycable#/");
    });
    afterEach(function () {
        //browser.executeScript('window.localStorage.clear()');
        browser.executeScript('window.sessionStorage.clear()');
    });
    it('should save searchParams (igus)', function () {
        //set values
        element(by.cssContainingText('option', 'Jetter')).click();
        element(by.cssContainingText('option', 'Motorleitung')).click();
        element(by.cssContainingText('option', '4-6')).click();
        m("sc.sp.length").clear();
        m("sc.sp.length").sendKeys("5");
        //search
        search("asd");
        //refresh
        refresh();
        //check if values were loaded correctly from session
        expect(m("sc.sp.producer").$('option:checked').getText()).toBe("Jetter");
        expect(m("sc.sp.category").$('option:checked').getText()).toBe("Motorleitung");
        expect(m("sc.sp.size").$('option:checked').getText()).toBe("4-6");
        expect(m("sc.sp.length").getAttribute('value')).toBe("5");
        expect(m("sc.sp.articleId").getAttribute('value')).toBe("asd");
    });
});
